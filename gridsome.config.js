const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')

module.exports = {
  siteName: 'Awesome Blog',
  siteDescription: 'Blog about awesome lists, collections of resources around a specific technology.',
  siteUrl: process.env.DEPLOY_URL || 'https://blog.awesome',
  metadata: {
    author: 'Simon Mannes',
    twitter: {
      site: '@simon_mannes',
      creator: '@simon_mannes'
    }
  },
  plugins: [
    {
      use: '@gridsome/vue-remark',
      options: {
        typeName: 'BlogPost',
        baseDir: './content/blog',
        route: '/:slug',
        template: './src/templates/BlogPost.vue',
        includePaths: ['./content/markdown-snippets'],
        plugins: [
          ['gridsome-plugin-remark-shiki', { theme: 'nord', skipInline: true }]
        ],
        refs: {
          tags: {
            typeName: 'Tag',
            create: true
          }
        }
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        exclude: ['/privacy', '/legal']
      }
    }
  ],
  templates: {
    // add the tags template
    Tag: '/tag/:id'
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          tailwindcss,
          autoprefixer
        ]
      }
    }
  }
}
