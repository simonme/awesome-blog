---
slug: "import-other-markdown-files"
date: "2021-03-28"
title: "How to import other markdown files"
tags: ["gridsome", "frontend", "markdown"]
summary: ""
---

import OfficeIpsum from '../markdown-snippets/officeipsum.md'

It's possible to import and then use markdown files inside other markdown files.
Two things are necessary for this to work:

1. A working [Vue Remark setup](https://www.npmjs.com/package/@gridsome/vue-remark)
2. Set up the [include path for Vue Remark](https://www.npmjs.com/package/@gridsome/vue-remark#includepaths)

---

<office-ipsum />
