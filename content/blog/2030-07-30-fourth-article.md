---
slug: "fourth-post"
date: "2030-07-30"
title: "The Future of Design"
tags: ["future", "design"]
summary: "Synth pinterest bespoke, taiyaki williamsburg chambray cloud bread readymade."
---

import SpoilerSafe from '~/components/SpoilerSafe.vue'

## This should be a heading 2

I'm baby chartreuse knausgaard gastropub deep v mlkshk pickled crucifix chicharrones meggings. Listicle jianbing tbh sriracha tofu, waistcoat post-ironic copper mug williamsburg scenester. Banh mi tilde swag beard. PBR&B disrupt affogato 8-bit fanny pack. Tacos fam brooklyn jean shorts. Taiyaki fam +1 tote bag chia palo santo.

**This is bold**

<spoiler-safe summary="Reveal spoiler">
Banh mi authentic fashion axe affogato shoreditch umami bicycle rights keytar put a bird on it drinking vinegar pitchfork taxidermy. Synth pinterest bespoke, taiyaki williamsburg chambray cloud bread readymade.
</spoiler-safe>

Single-origin coffee lyft iPhone street art, hot chicken yr live-edge gentrify waistcoat.

## This should be a heading 2

> This should be a block quote

Selvage twee viral, lyft chartreuse swag crucifix hexagon lo-fi meggings literally. Jianbing knausgaard vexillologist, sustainable yr twee tote bag cray keytar schlitz slow-carb DIY dreamcatcher brooklyn listicle. Chambray letterpress flexitarian meditation gentrify. Single-origin coffee lyft iPhone street art, hot chicken yr live-edge gentrify waistcoat.

_Source: [Hipster Ipsum](https://hipsum.co)_

