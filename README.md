# Mannes.tech Gridsome tutorial

You can find the tutorial series here: [https://mannes.tech/gridsome-tutorial](https://mannes.tech/gridsome-tutorial).

## Run the project

1. Install the dependencies by running `yarn install`
2. Either run local dev mode with `yarn develop` or a production build with `yarn build`

## Storybook

> Storybook is currently broken. I'll update the instructions here as soon as Storybook is working again!
