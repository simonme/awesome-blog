import ThemeSwitcher from './ThemeSwitcher'

export default {
  title: 'Components/ThemeSwitcher',
  component: ThemeSwitcher
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ThemeSwitcher },
  template:
    '<theme-switcher :theme="theme" />'
})

export const Light = Template.bind({})
Light.args = {
  theme: 'theme-light'
}

export const Dark = Template.bind({})
Dark.args = {
  theme: 'theme-dark'
}
